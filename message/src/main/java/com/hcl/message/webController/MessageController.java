package com.hcl.message.webController;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.message.domain.Message;

@RestController
@RequestMapping("/message")
public class MessageController {
	
	@GetMapping
	public String message() {
		return "Hello World...!!";
	}
	
	@GetMapping("/query")
	public Message query(@RequestParam("id") int id,@RequestParam("msg") String message) {
		Message msg = new Message();
		msg.setId(id);
		msg.setMessage(message);
		
		return msg;
		
	}

}
